//1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//у кожного об'єкту є прототип. у випадку коли при зверненні до методу чи властивості саме об'єкту його/її не було виявлено,
//джаваскрипт починає шукати цей метод чи властивість у його прототипі, підіймаючись по ланцюгу прототипів угору, поки не знайде необхідне чи не досягне верхнього рівня

//2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
//для ініціалізаціїї й використання властивостей батьківського класу для класу-нащадка


class Employee {
    constructor(name, age, salary) {
       this._name = name;
       this._age = age;
       this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }
    
    get salary() {
        return this._salary;
    }

    set name(name) {
        this._name = name;
    }

    set age(age) {
        this._age = age;
    }

    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
       super(name, age, salary);
       this._lang = lang;
    }

    get salary() {
        return (super.salary*3);
    }
}

const programmer1 = new Programmer("Ivan", 30, 1000, ["English", "Ukrainian"]);
const programmer2 = new Programmer("Nver", 18, 200, ["Armenian", "English"]);
const programmer3 = new Programmer("Olga", 22, 500, ["Ukrainian", "Armenian"]);
console.log(programmer1, programmer1.salary);
console.log(programmer2, programmer2.salary);
console.log(programmer3, programmer3.salary);